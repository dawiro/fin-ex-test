CREATE USER 'finex'@'localhost' IDENTIFIED BY 'w2hG8ill';
CREATE DATABASE IF NOT EXISTS `finex`;
CREATE TABLE IF NOT EXISTS `finex`.`submissions` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`submission_name` VARCHAR(32) NOT NULL,
	`submission_email` VARCHAR(32) NOT NULL,
	`submission_password` VARCHAR(32) NOT NULL,
	PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
GRANT ALL PRIVILEGES ON finex.* TO 'finex'@'localhost';
FLUSH PRIVILEGES;