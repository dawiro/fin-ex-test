<?php
/**
 * Represents the form submission from the register form.
 * 
 * Simple class to add our submission to a database and send a confirmation email.
 *
 */
class FormSubmission {
    /**
     * The name submitted via the form.
     * @var string
     */
    protected $submission_name;

    /**
     * The email address submitted via the form.
     * @var string
     */
    protected $submission_email;

    /**
     * The password submitted via the form.
     * @var string
     */
    protected $submission_password;

    /**
     * Assign our required attributes.
     * Note the dependency injection as we receive the database object.
     * 
     * @param   string  $submission_name
     * @param   string  $submission_email
     * @param   string  $submission_password
     * @param   object  $db
     */
    public function __construct($submission_name='', $submission_email='', $submission_password='', Database $db){
        $this->submission_name=$submission_name;
        $this->submission_email=$submission_email;
        $this->submission_password=$submission_password;
        $this->db=$db;
    }

    /**
     * Add our submission to the database.
     * We sanitise our attributes to prevent SQL injection.
     * 
     * @return  mysqli result object
     */
    public function addSubmission(){
        $submission_name = $this->db->sanitiseValue($this->submission_name);
        $submission_email = $this->db->sanitiseValue($this->submission_email);
        $submission_password = $this->db->sanitiseValue($this->submission_password);
        $query="INSERT INTO submissions (submission_name, submission_email, submission_password) VALUES ('$submission_name', '$submission_email', '$submission_password')";
        $result = $this->db->query($query);
        return $result;
    }

    /**
     * Sends a confirmation email confirming that this submission was successful.
     * 
     * @param   object
     * @return  Boolean
     */
    public function sendConfirmationEmail(Mail $mail) {
        $result = $mail->sendEmail($this->email, 'This is to confirm that your registration was successful.');
        return $result;
    }
}