<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fin-Ex Test</title>
    <link rel="stylesheet" href="dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/app.min.css">
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-sm">
            <div class="container">
                <a class="navbar-brand" href="index.php">
                    <img class="fin-ex-logo" src="images/logo-blue.png" />
                </a>
                <div>
                    <ul class="nav navbar">
                        <li class="nav-item page1-nav-item active">
                            <a class="nav-link page1-link" href="#">Page 1</a>
                        </li>
                        <li class="nav-item page2-nav-item">
                            <a class="nav-link page2-link" href="#">Page 2</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <div id="page1">
        <div id="carousels-container">
            <div class="carousel-flexbox-container"></div>
        </div>
    </div>

    <div class="container" id="page2">
        <div class="property-list-container">
            <h1>Portfolio Explorer</h1>
            <p>Click on our portfolio items below in order to explore further.</p>
            <ul id="property-list">
                <li class="level-1">Harrington
                <ul>
                    <li class="level-2">Property 1
                    <ul>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 1</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 2</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 3</a></li>
                    </ul>
                    </li>
                    <li class="level-2">Property 2
                    <ul>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 1</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 2</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 3</a></li>
                    </ul>
                    </li>
                    <li class="level-2">Property 3
                    <ul>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 1</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 2</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 3</a></li>
                    </ul>
                    </li>
                </ul>
                </li>
                <li class="level-1">Cornwall
                <ul>
                    <li class="level-2">Property 1
                    <ul>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 1</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 2</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 3</a></li>
                    </ul>
                    </li>
                    <li class="level-2">Property 2
                    <ul>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 1</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 2</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 3</a></li>
                    </ul>
                    </li>
                    <li class="level-2">Property 3
                    <ul>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 1</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 2</a></li>
                        <li><a href="https://fin-ex.com/portfolio-items/property-luxury-serviced-apartments-in-london/" target="_blank">Room 3</a></li>
                    </ul>
                    </li>
                </ul>
                </li>
            </ul>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="border row">
                <div class="col-sm-5 d-flex align-items-center"">
                    <div>
                        <p>To register with us, just fill in our form.</p>
                        <p>Please note that all fields are mandatory.</p>
                    </div>
                </div>
                <div class="col-sm-7">
                    <form id="fin-ex-registration-form">
                        <div class="form-group my-2 row">
                            <label class="col-form-label col-sm-4" for="form-name">Name</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="form-name" name="form-name" type="text">
                                <span class="text-danger" id="form-name-warning">&nbsp;</span>
                            </div>
                        </div>
                        <div class="form-group my-2 row">
                            <label class="col-form-label col-sm-4" for="form-email">Email</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="form-email" name="form-email" type="text">
                                <span class="text-danger" id="form-email-warning">&nbsp;</span>
                            </div>
                        </div>
                        <div class="form-group my-2 row">
                            <label class="col-form-label col-sm-4" for="form-password">Password</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="form-password" name="form-password" type="password">
                                <span class="text-danger" id="form-password-warning">&nbsp;</span>
                            </div>
                        </div>
                        <div class="form-group my-2 row">
                            <label class="col-form-label col-sm-4" for="form-validate-password">Validate Password</label>
                            <div class="col-sm-8">
                                <input class="form-control" id="form-validate-password" name="form-validate-password" type="password">
                                <span class="text-danger" id="form-validate-password-warning">&nbsp;</span>
                            </div>
                        </div>
                        <div class="my-4 row">
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-success" id="form-register-button">Register</button>
                            </div>
                            <div class="col-sm-8" id="form-submission-feedback">

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </footer>
    <script src="dist/js/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="dist/js/app.min.js"></script>
</body>
</html>