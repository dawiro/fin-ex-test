<?php
/**
 * Represents our database connection.
 * 
 * A larger application would have DB connection details in a config file. For our purposes, we hardcode here.
 */
class Database{
    /**
     * We will assign our database connection to this attribute.
     * @var object
     */
    private $dbConnection;

    /**
     * Database host name
     * @var string
     */
    private $dbHost='localhost';

    /**
     * Name of the database user.
     * @var string
     */
    private $dbUsername='finex';

    /**
     * Password of the database user.
     * @var string
     */
    private $dbPassword='w2hG8ill';

    /**
     * Name of the database.
     * @var string
     */
    private $dbDatabase='finex';

    /**
     * Establish our database connection.
     */
    public function __construct(){
        $this->dbConnection = mysqli_connect($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbDatabase);
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
    }

    /**
     * Execute our query. There are different ways of approaching the issue of executing DB queries through dependency injection but this way is perhaps simplest and, because it is generic rather than specific, conforms to loose coupling.
     * 
     * @param   string  $query
     * @return  mysqli query object
     */
    public function query($query){
        return mysqli_query($this->dbConnection, $query);
    }

    /**
     * Sanitises a value to prevent SQL injection.
     * We could use prepared statements, of course, but this is simpler, just as valid and, again, conforms to loose coupling (prepared statements require assumptions about the type of query).
     * 
     * @param   string  $value
     * @return  string
     */
    public function sanitiseValue($value){
        return $this->dbConnection->real_escape_string($value);
    }
}