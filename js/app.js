/**
 * For a normal application, we would use ES6 module functionality to import functionality. Since there is no code re-use in this micro project, I'm keeping things simple by having all our code in one file.
 * 
 * All our event-driven JQuery and JS code sits in our document onload block below. This handles the registation form, portfolio explorer and menu navigation.
 * Below that is a helper function to check email address syntax
 * Finally, we have OO classes to implement the carousel functionality.
 */

/**
 * This block contains our event-driven JS for form validation and navigating our Page 2Property Explorer.
 */
$(function(){
    /**
     * REGISTRATION FORM VALIDATION AND SUBMISSION
     */

    //Add an event listener that handles field validation on our registration form submission.
    let finExRegistrationForm = document.getElementById('fin-ex-registration-form');
    finExRegistrationForm.addEventListener('submit', event => {
        event.preventDefault();

        let formName = $('#form-name').val();
        let formEmail = $('#form-email').val();
        let formPassword = $('#form-password').val();
        let formValidatePassword = $('#form-validate-password').val();

        let errorFlag=false; //Set to true below if error(s) found.

        if(formName==''){
            $('#form-name-warning').html('You must enter a name.');
            $('#form-name').addClass('border-danger');
            errorFlag=true;
        }else{
            $('#form-name-warning').html('&nbsp;');
            $('#form-name').removeClass('border-danger');
        }

        if(validateEmail(formEmail)) {
            $('#form-email-warning').html('&nbsp;');
            $('#form-email').removeClass('border-danger');
        }else{
            $('#form-email-warning').html('You must enter a valid email address.');
            $('#form-email').addClass('border-danger');
            errorFlag=true;
        }

        if(formPassword.length < 7){
            $('#form-password-warning').html('Your password must be at least 6 characters long.');
            $('#form-password').addClass('border-danger');
            errorFlag=true;
        }else{
            $('#form-password-warning').html('&nbsp;');
            $('#form-password').removeClass('border-danger');   
        }

        if(formValidatePassword !== formPassword){
            $('#form-validate-password-warning').html('Validate password does not match password.');
            $('#form-validate-password').addClass('border-danger');
            errorFlag=true;
        }else{
            $('#form-validate-password-warning').html('&nbsp;');
            $('#form-calidate-password').removeClass('border-danger');   
        }

        var dataString=`form_name=${formName}&form_email=${formEmail}&form_password=${formPassword}&form_validate_password=${formValidatePassword}`;

        /**
         * If the form submission is error-free, submit to our PHP script which a) inserts the data into a database table, b) sends the user a confirmation email.
         * If the DB insertion and emailing work, a success message is displayed.
         * If there were errors with either, we display the errors.
         */
        if(errorFlag===false){
            $.ajax({
                url:'ajax_handle_form_submission.php',
                type:'POST',
                data: dataString,
                success:function(result){
                    if(result==='success'){
                        $('#form-register-button').fadeOut();
                        $('#form-feedback').addClass('text-success').html('Thank you. We have sent a confirmation email to the address you supplied.');
                    }else{
                        $('#form-submission-feedback').addClass('text-danger').html(result);
                    }
                }
            });
        }
    });


    /**
     * PORTFOLIO EXPLORER
     * 
     * Sits on page 2 of the site.
     * The idea is that we present a user with a tree view of locations->properties->rooms so that a user can, in a hierarchical way, easily explore a portfolio.
     * It's not technically complex but it was the most practical, relevant use of JQuery I could think of - a useful tool that helps a user navigate in a more delightful way than clicking through pages.
     */

    //Find every list item in our entire portfolio list and add a click event that calls slideToggle.
    $('#property-list li').on('click', function (event) {
        //Because we have nested elements, we need stopPropagation() to prevent all elements in the nest being affected by a click.
        event.stopPropagation();
        //We only want the nested elements immediately below of this list item to be displayed/hidden, so the slideToggle() only gets applied to this element's children.
        $(this).children('ul').slideToggle();
    });

    /**
     * MENU FUNCTIONALITY
     * 
     * For a two page site it is easier to implement page changes in JS rather than in PHP (as one normally would for a PHP backend project), especially since we don't want to build PHP routing for a two page project.
     */

    //Page 1 clicked? Highlight menu option and display page one.
    $('.page1-link').on('click', function (event) {
        $('.page2-nav-item').removeClass('active');
        $('.page1-nav-item').addClass('active');
        $('#page2').fadeOut(function(){
            $('#page1').fadeIn();
        });
    });

    //Page 2 clicked? Highlight menu option and display page two.
    $('.page2-link').on('click', function (event) {
        $('.page1-nav-item').removeClass('active');
        $('.page2-nav-item').addClass('active');
        $('#page1').fadeOut(function(){
            $('#page2').fadeIn();
        });
    });
});


/**
 * This is a helper function which we use for the form validation above.
 * Determine whether the passed string is a syntactically valid email address.
 * 
 * @param   {string}
 * @return  {boolean}
 */
function validateEmail(email) {
    const res = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return res.test(String(email).toLowerCase());
}


/**
 * CAROUSEL FUNCTIONALITY
 * 
 * We need OO JS for the carousels. Below we have:
 * The FinExCarousel class. Each object represents a carousel.
 * The ApiHandler class. Handles the consumption of the REST API.
 * Program logic - instantiate our classes and use them.
 */

/**
 * Handles carousel functionality.
 */
class FinExCarousel {

    /**
     * Initialise our variables.
     * We have dependency injection as we pass in our apiHandler.
     * We're also constructing a template string so we can dynamically change each carousel.
     * 
     * @param {string}  location 
     * @param {integer} startAt 
     * @param {object} carouselApiHandler 
     */
    constructor(location, startAt, carouselApiHandler){
        this.startAt = startAt;
        this.carouselApiHandler = carouselApiHandler;
        this.currentSlide=1;
        this.location = location;
        this.locationTitle = this.location.toUpperCase();
        this.carouselTemplate=`
            <div class="carousel-flexbox-container carousel-title">
                <h2>${this.locationTitle}</h2>
            </div>
            <div class="carousel-flexbox-container">
                <div class="carousel-container">
                    <div class="carousel-slides-container">
                        <img alt="" class="${this.location}_carouselSlide ${this.location}_image_1" src="">
                        <img alt="" class="${this.location}_carouselSlide ${this.location}_image_2" src="">
                        <img alt="" class="${this.location}_carouselSlide ${this.location}_image_3" src="">
                        <img alt="" class="${this.location}_carouselSlide ${this.location}_image_4" src="">
                        <img alt="" class="${this.location}_carouselSlide ${this.location}_image_5" src="">
                    </div>
                    <div class="carousel-slide-title-container">
                        <div class="${this.location}-carousel-slide-title"></div>
                    </div>
                    <div class="carousel-buttons-container">
                        <div class="carousel-button ${this.location}-button-previous">&#10094;</div>
                        <div class="carousel-button ${this.location}-button-next">&#10095;</div>
                    </div>
                </div>
            </div>
        `;
    }

    /**
     * Adds our carousel to the page.
     * 
     * Create a HTML node, populate it and add it to the page.
     * Use the apiHandler object to fetch the photos from a REST API.
     * Add listeners for the carousel navigation.
     * Populate the carousel with the fetched images and descriptions.
     */
    displayCarousel(){
        let div = document.createElement('div');
        div.innerHTML = this.carouselTemplate;
        let carouselsContainer = document.getElementById('carousels-container');
        carouselsContainer.appendChild(div);
        this.carouselApiHandler.fetchPhotos(this.startAt, this.location);
        this.addCarouselEventListeners();
        this.displaySlides(this.currentSlide);
    }

    /**
     * Add a couple of event listeners which let our carousel navigation buttons move slides back and forth.
     */
    addCarouselEventListeners(){
        let buttonPreviousClass = `.${this.location}-button-previous`;
        let buttonNextClass = `.${this.location}-button-next`;

        document.querySelector(buttonPreviousClass).addEventListener('click', ()=>{
            this.moveSlide(-1);
        });
        
        document.querySelector(buttonNextClass).addEventListener('click', ()=>{
            this.moveSlide(1);
        });
    }

    /**
     * Move to the desired slide.
     * 
     * @param {integer} newCurrentSlideValue
     */
    displaySlides(newCurrentSlideValue) {
        let carouselSlideClassName = `${this.location}_carouselSlide`;
        let carouselSlideClassNameWithPrefix = `.${this.location}_carouselSlide`;
        let carouselSlideTitleClassNameWithPrefix = `.${this.location}-carousel-slide-title`;

        //Get all the slides.
        let allSlides = document.getElementsByClassName(carouselSlideClassName);
    
        //If the new current slide value is greater than the number of slides, we go back to the start by setting the current slide value to 1.
        if (newCurrentSlideValue > allSlides.length) {
            this.currentSlide = 1;
        }
    
        //If the new current slide value is less than 1 (i.e. we were on slide 1 and tried to go to the previous slide), we go to the last slide (i.e. we wrap around).
        if (newCurrentSlideValue < 1) {
            this.currentSlide = allSlides.length;
        }
        
        //Hide all the slides.
        $(carouselSlideClassNameWithPrefix).css('display', 'none');

        //Display the current slide.
        allSlides[this.currentSlide-1].style.display = "block";

        //Populate the title field for the current slide.
        $(carouselSlideTitleClassNameWithPrefix).html(allSlides[this.currentSlide-1].alt);
    }

    /**
     * A wrapper for calling the method that moves the slides.
     * The wrapper literally just lets us modify the slide number being passed.
     * 
     * @param {*} newCurrentSlideValue 
     */
    moveSlide(newCurrentSlideValue) {
        this.displaySlides(this.currentSlide += newCurrentSlideValue);
    }
}

/**
 * A class to handle fetching image URLs and descriptions from the REST API.
 * 
 * The apiHandler class is not quite an adapter pattern (because we're not implementing third party methods and REST provides a consistent API) but it's serving a similar purpose - if the API changes, we just need to change this class.
 */
class apiHandler{
    /**
     * We're hardcoding the REST endpoint and the number of records to fetch. In a proper application these values would be passed in or, more likely, be taken from a config file.
     */
    constructor(){
        this.baseUrl = 'http://jsonplaceholder.typicode.com/photos';
        this.limit = 5;
    }

    /**
     * Make a request to the REST API, consume the returned JSON.
     * 
     * We are using the new Fetch API rather than the traditional XHR object.
     * This is, therefore, an aysnc method, so we handle the response by injecting the image URL and image description directly into the carousel on the page.
     * Note that the REST API allows us to limit the records returned, so each request only fetches the JSOn needed for that particular carousel (a batch of 5 records). This feels more natural than fetching all the photos for all carousels with one request, then getting into destructuring operations.
     * 
     * @param {*} startAt 
     * @param {*} location 
     */
    fetchPhotos(startAt, location){       
        var fetchUrl = `${this.baseUrl}?_start=${startAt}&_limit=${this.limit}`;
        fetch(fetchUrl)
            .then((response) => response.json())
            .then((json) => {
                let i=1;
                for(const photo of json){
                    let imageClass=`.${location}_image_${i}`;
                    let slideTitleClass = `.${location}-carousel-slide-title`;
                    $(imageClass).attr('src', photo.url);
                    $(imageClass).attr('alt', photo.title);
                    $(slideTitleClass).html(photo.title);
                    i++;
                }
            })
            .catch(function(error) {
                console.log('Error was' + error);
            });
    }
}


/**
 * The actual code for creating our carousels.
 */

//Instantiate our apiHandler class.
let carouselApiHandler = new apiHandler();

/**
 * We require one individual carousel per location. The code below might look inefficient due to repetition but it is the correct use of objects - each of our carousel objects has its own location and its own images, and is therefore a proper, individual instance of our class.
 * Could we improve this with a pattern? For a proper site, probably, but it feels like overkill to start decoupling and subclassing everything for half a dozen carousels.
 * For each carousel, we pass in the location, the offset for our REST API requests (i.e. we want a different batch of image URLs for each carousel), and we inject our apiHandler object that will actually do the fetching from the REST API.
 */

let carouselHarrington = new FinExCarousel('harrington', 0, carouselApiHandler);
carouselHarrington.displayCarousel();

let carouselCornwall = new FinExCarousel('cornwall', 5, carouselApiHandler);
carouselCornwall.displayCarousel();

let carouselSouthwell = new FinExCarousel('southwell', 10, carouselApiHandler);
carouselSouthwell.displayCarousel();

let carouselMews = new FinExCarousel('mews', 15, carouselApiHandler);
carouselMews.displayCarousel();

let carouselKensington = new FinExCarousel('kensington', 20, carouselApiHandler);
carouselKensington.displayCarousel();

let carouselTourist = new FinExCarousel('tourist', 25, carouselApiHandler);
carouselTourist.displayCarousel();

let carouselApartments = new FinExCarousel('apartments', 30, carouselApiHandler);
carouselApartments.displayCarousel();