# FIN-EX TEST

## Description

A small PHP/Javascript/MySQL Web application created as part of a technical test.

The application comprises two pages.

Page one displays several carousels. The styling is minimal due both to time constrains and since the purpose of the task is to demonstrate how to ingest a REST API

Page two displays a tree hierarchy that you can click on to expand each node. Again, the styling is minimal due to time constraints but it illustrates how JQuery can be used to traverse DOM nodes and produce an interactive site component that encourages user interaction. Given more time and also media assets, more pleasing animations could be added and the leaf nodes could, instead of displaying a hyperlink to the Fin-Ex site, have an interactive element or modal that would enable a user to explore further.

## Prerequisites

You will need a LAMP server with Node (specifically, NPM) installed.

## Installation Instructions

1. Clone the repository: **git clone https://dawiro@bitbucket.org/dawiro/fin-ex-test.git**
2. cd into the project folder: **cd fin-ex-test**
3. Create the MySQL user (finex), database (finex) and table (submissions) by importing fin-ex-test.sql. This will be something like: **mysql < fin-ex-test.sql**
4. Install all the dependencies for a production environment: **npm install --production**
5. To generate the files needed for production (these are created in the /dist folder): **npm run build**
6. The entry point for the application is index.php in the project root folder, so if you point a URL at that folder on your server, the application will run.

## Technical Notes

- While I have experience with unit testing with PHPUnit, there is not enough code to produce meaningful PHP tests for this project. Also, unit testing Javascript is not in my skillset. Therefore, there is no test script in the task runner.
- I've thoroughly commented the code in order to explain my decisions and to demonstrate my understanding of the code. I would probably be less verbose on an actual project.
- Due to time constraints, I've had to focus on completing the brief rather than taking time to improve the user interface or build an application intended for future re-use. This has meant minimal styling and, more importantly, employing one-off code approaches rather best practice. For instance, database connection details are hardcoded into Database.php rather than being present in a separate configuration file. Passwords are stored in cleartext in the database table rather than being hashed.
- I haven't used SCSS. I personally prefer coding in CSS, and the tiny size of this project does not warrant a precompiler anyway.
- Since HTTP/2 and the parallel downloads made possible by multiplexing, single file bundling on build tasks is often considered an anti-pattern, therefore my build tasks do not concatenate files.

## Code Overview
- **/css** - source CSS folder
- **/dist** - production-ready files are generated here by the build task.
- **/images** - well, image!
- **/js** - source JS folder
- **/js/app.js** - All of the JS for the project is here. For a real-world project that requires code re-use, this would be split up into multiple files and imported as modules. For this project it was faster for me to have everything in a single file.
- **.gitignore** - Excludes /dist and /node_modules.
- **ajax_handle_form_submission.php** - When the registration form submits, this is the file it makes the AJAX request to. It instantiates three classes found in Database.php, FormSubmission.php and Mail.php.
- **Database.php** - Class file. DB connection details are hardcoded here and it handles the insertion of register form data into the MySQL database.
- **fin-ex-test.sql** - SQL script to create the user, database and table needed to record the input from the registration form.
- **FormSubmission.php** - Class file. The registration form submission is conceptualised as an entity, and that is what this class represents. It uses the Database and Mail objects via dependency injection, so that technical implementaton is decoupled.
- **index.php** - The landing page for the site. The two menu options at the top are not true pages - it was easier to have Javascript change the page content on a single page rather than have another PHP file representing the second page.
- **Mail.php** - Class file. The FormSubmission class needs to inform the user that their registration was successful. How this is done is not the concern of the FormSubmission class, hence the Mail class takes care of its implementation.
- **package.lock.json** - All the NPM dependencies.
- **package.json** - I've used NPM as my task runner and my simple build scripts can be found in the scripts section.
- **README.md** - You're reading it!