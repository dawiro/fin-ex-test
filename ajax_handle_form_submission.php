<?php
/**
 * When the registration form is submitted, the AJAX request points to this PHP script.
 * We do some validation of the form data that was submitted.
 * If errors were found, we return an error string.
 * If no errors were found, we use our database object to insert the details into a simple database.
 * If the database insert worked, we send the user a confirmation email.
 * The script will then return (echo) either a success message or an error message, which is used to provide a response on the form.
 */

//Normally we would autoload dependencies via Composer but for this small project we'll do it old school.
require 'Database.php';
require 'FormSubmission.php';
require 'Mail.php';

//Fetch the fields passed by the AJAX.
$name=$_POST['form_name'];
$email=$_POST['form_email'];
$password=$_POST['form_password'];
$validatePassword=$_POST['form_validate_password'];

//We have user-generated input so since that is a security risk, sanitise immediately rather than passing to some validation object.

//Store any errors we find during validation in our string.
$errorString = '';

//We've done client-side validation, now for server-side.
if(strlen($name)===0){
    $errorString.='<br />Invalid name';
}

if(filter_var($email, FILTER_VALIDATE_EMAIL)===false){
    $errorString.='<br />Invalid email address';
}

if(strlen($password)===0 || $password !== $validatePassword){
    $errorString.='<br />Invalid password';
}

//If errors were found, return them for output on the form, otherwise add the submission to the database.
if(strlen($errorString)===0){
    //Instantiate our Database object.
    $db = new Database();
    //Instantiate our FormSubmission object. Pass validated fields and dependency inject the DB object.
    $formSubmission = new FormSubmission($name, $email, $password, $db);
    $dbSubmissionResult = $formSubmission->addSubmission();
    //We only send a confirmation email if the database submission worked.
    if($dbSubmissionResult===true){
        //We only need the Mail class if our database submission was successful, so we instantiate here.
        $mail = new Mail();
        $mailSubmissionResult = $formSubmission->sendConfirmationEmail($mail);
        if($mailSubmissionResult===true){
            echo 'success';
        }else{
            echo 'You have been registered but there was a problem sending your confirmation email.';
        }
    }else{
        echo 'There was a problem saving your registration details.';
    }
}else{
    echo $errorString;
}