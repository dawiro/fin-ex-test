<?php
/**
 * Handles the sending of the email confirmation once the registration form data is inserted into the database.
 */
class Mail{
    /**
     * Email has already been validated so no need to add email injection sanitisation.
     * 
     * @param   string  $email
     * @param   string  $message
     * @return  Boolean
     */
    public function sendEmail($email, $message){
        $to = $email;
        $subject = "fin-ex confirmation email";
        
        $message = "<div><b>$message</b></div>";
        
        $header = "From:no-reply@fin-ex.com \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-type: text/html\r\n";
        
        $result = mail ($to,$subject,$message,$header);
        
        return $result;
    }
}